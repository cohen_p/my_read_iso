CFLAGS= -pedantic -Wall -Werror -Wextra -std=c99
CC=gcc
SRC=src/my_read_iso.c src/string_function.c src/position_function.c src/utils.c src/command.c
BIN=my_read_iso

all: $(BIN)

clean:
	$(RM) $(BIN)

$(BIN): clean $(SRC)
	$(CC) $(CFLAGS) $(SRC)  -o $@

