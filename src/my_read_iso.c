#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include "iso9660.h"
#include "string_function.h"
#include "position_function.h"
#include "utils.h"
#include "command.h"

void cmd(char *position_root, struct iso_prim_voldesc *iso, void *pmap)
{
  char buff[2][64] = {{0}, {0}};
  char buf_trash[64];
  char *position_current = position_root;
  while(1)
  {
    clean_buffer(buf_trash);
    shell(buf_trash);
    parse(buf_trash, buff);
    if (exec_cmd(buff, buf_trash, position_root, &position_current, iso, pmap) == 1)
      break;
  }
}

static int aux(int fd)
{
  close(fd);
  return 0;
}

int main(int argc, char *argv[])
{
  for (int i = 2; i < my_strlen(argv[0]); i++)
    bin_name[i-2] = argv[0][i];
  char buff_line[64];

  if (argc != 2)
  {
    fprintf(stderr, "usage: %s FILE\n", bin_name);
    return 0;
  }
  int fd = open(argv[1], O_RDONLY);
  if (fd == -1)
  {
    fprintf(stderr,"%s: %s: No such file or directory\n", bin_name, argv[1]);
    close(fd);
    return 1;
  }
  void *pmap = position_mmap(fd, bin_name, argv[1]);
  if (pmap == NULL)
    return 1;
  char *cp = pmap;
  cp += ISO_PRIM_VOLDESC_BLOCK * ISO_BLOCK_SIZE; 
  void *tmp = cp;
  struct iso_prim_voldesc *iso = tmp;
  if (my_strncmp(iso->std_identifier, "CD001", 5) == 1)
  {
    fprintf(stderr, "%s: %s: invalid ISO9660 file\n", bin_name, argv[1]);
    return aux(fd) + 1;
  }
  void *cast_void = &(iso->root_dir);
  char *current_dir = cast_void;
  char *position_root = my_current_dir(pmap,current_dir);
  int filed = open("commands", O_RDONLY);
  if (filed == -1)
  {
      close(filed);
      return 1;
  }

  if (!isatty(0))
    isnotatty(buff_line, filed, position_root, iso, pmap);
  else
    cmd(position_root, iso, pmap);
  return aux(fd);
}
