#include "string_function.h"
#include <stdio.h>

int my_strlen(char s1[])
{

  int count = 0;
  int i = 0;
  while ( s1[i] != '\0')
  {
      count += 1;
      i++;
  }
  return count;
}
int my_strcmp(char s1[], char s2[])
{
    int i = 0;
    while (s1[i] != '\0' && s2[i] != '\0')
    {
        if (s1[i] != s2[i])
            return 1;
        i++;
    }
    if (s1[i] != '\0' || s2[i] != '\0')
        return 1;
    return 0;
}

int my_strncmp(char s1[], char s2[], int n)
{
  int i = 0;
  while (s1[i] != '\0' && s2[i] != '\0' && i != n)
  {
    if (s1[i] != s2[i])
      return 1;
    i++;
  }
  if (i == n)
    return 0;
  return 1;
}


void print_char (char *string,int size)
{
    int i = 0;
    while (i != size)
    {
        printf("%c",string[i]);
        i++;
    }
    printf("\n");
}
