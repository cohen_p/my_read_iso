#include "utils.h"
#include "string_function.h"
#include <stdio.h>
#include <unistd.h>

void *offset_ptr(void *ptr, int64_t offset)
{
  char *bytes_ptr = ptr;
  bytes_ptr += offset;
  return bytes_ptr;
}

void parse(char buf_trash[64], char buff[][64])
{
  int i = 0;
  int j = 0;
  clean_buffer(buff[0]);
  clean_buffer(buff[1]);
  while (buf_trash[i] == ' ') //supprime espace AVANT la première lettre
    i++;

  while (i < 64 && buf_trash[i] != ' ') //stocke dans buff[0] la CMD
    buff[0][j++] = buf_trash[i++];
  buff[0][j] = '\0';

  while (buf_trash[i] == ' ') //supprime les espaces entre CMD et PARAM
    i++;

  j = 0;
  while(i < 64) // stocke dans buff[1] list de PARAM
    buff[1][j++] = buf_trash[i++];
  buff[1][j] = '\0';

  for (int i = my_strlen(buff[1]); i > 0; i--)  // Supprime les espaces APRES PARAM
  {
    if (buff[1][i] == ' ')
        buff[1][i] = '\0';
    else
        break;
  }
}

void clean_buffer(char *buff)
{
	for (int i = 0; i < 64; i++)
		buff[i] = 0;
}

char *clean_buf_trash(char *buff_trash)
{
  int i = 0;
  while (i < 64 && buff_trash[i] == ' ') //delete espace before
    i++;
  int j = my_strlen(buff_trash); // my_strlen
  while (j >= 0 && (buff_trash[j] == '\0' || buff_trash[j] == ' ')) //delete espace after
    j--;

  int l =  0;
  for (int k = i; k <= j; k++)
    buff_trash[l++] = buff_trash[k];
  buff_trash[l] = '\0';
  return buff_trash;
}
void my_getLine(char buff_line[][64], int fd)
{
  int i = 0;
  int j = 0;
  int nb_line = 0;
  char buff[64] = "";
  lseek(fd, 0,SEEK_SET); 
  int r = read(fd, buff, 64);
  while (i < r)
  {
    if (buff[i] == '\n')
    {
      if (buff[i + 1] == '\n')
        nb_line++; 
      else if (buff[i + 1] == '\0')
        buff_line[nb_line][j + 1] = '\0';
      else
      {
        nb_line++;
        j = 0;
        i++;
      }
    }
    buff_line[nb_line][j++] = buff[i++];
  }
}
