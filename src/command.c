#include "command.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include "iso9660.h"
#include "string_function.h"
#include "position_function.h"
#include "utils.h"

void info(struct iso_prim_voldesc *iso)
{
  int i = 0;
  printf ("System Identifier: ");
  while (iso->syidf[i] != ' ')
  {
    printf("%c", iso->syidf[i]);
    i++;
  }
  printf("\n");
  printf("Volume Identifier: ");
  printf("%s", iso->vol_idf);
  printf("\n");
  printf("Block count: %d\n", iso->vol_blk_count.le);
  printf("Block size: %d\n", iso->vol_blk_size.le);
  printf("Creation date: ");
  print_char(iso->date_creat,ISO_PRIM_VOLDESC_BLOCK);
  printf("Application Identifier: ");
  print_char(iso->app_idf, ISO_APP_LEN);
}



void ls(char *repertory)
{
  int j = 0;
  while(1)
  {
    void *pv = repertory;
    struct iso_dir *rep = pv;
    if (rep->dir_size == 0)
      break;
    char hidden = '-';
    char directory = '-';
    if (1 & rep->type)
      hidden = 'h';
    if (2 & rep->type)
      directory = 'd';
    printf("%c%c", directory, hidden);
    printf("%9u ", rep->file_size.le);
    if (j == 0)
      printf(".");
    if (j == 1)
      printf("..");
    char buff_idf[rep->idf_len];
    my_idf(rep,buff_idf);
    printf("%s\n", buff_idf);
    repertory += rep->dir_size;
    j++;
  }
}



char *cd(void *pmap, char *position_root, char *position_current,
  char buff_clean[64])
{
  if (my_strcmp(buff_clean, "") == 0)
  {
    fprintf(stdout, "Changing to 'root dir' directory\n");
    return position_root;
  }
  char *position_backup = position_current;
  while (1)
  {
    void *pv = position_current;
    struct iso_dir *rep = pv;
    if (rep->dir_size == 0)
      break;
    if (!(rep->type & iso_file_isdir))
    {
      fprintf(stderr, "entry '%s' is not a directory\n", buff_clean);
      return position_backup;
    }
    char buff_idf[rep->idf_len + 1];
    my_idf(rep,buff_idf);
    if (my_strcmp(buff_clean, buff_idf) == 0)
    {
      fprintf(stdout, "Changing to '%s' directory\n", buff_clean);
      return  my_current_dir(pmap, position_current);
    }
    position_current += rep->dir_size; 
  }
  if (position_backup)
    fprintf(stderr, "unable to find '%s' directory\n", buff_clean);
  return position_backup;
}



void cat(void *pmap, char *position_current, char buff_clean[64])
{
  while (1)
  {
    void *pv = position_current;
    struct iso_dir *rep = pv;
    if (rep->dir_size == 0)
      break;
    char buff_idf[rep->idf_len + 1];
    my_idf(rep,buff_idf);
    if (my_strcmp(buff_clean, buff_idf) == 0)
    {
      if (2 & rep->type)
      {
        fprintf(stderr, "entry '%s' is a directory\n", buff_clean);  
        return;
      }
      position_current = my_current_dir(pmap, position_current);
      write(1, position_current, rep->file_size.le);
      return;
    }
    position_current += rep->dir_size; 
  }
    fprintf(stderr, "unable to find '%s' entry\n", buff_clean);
}



void get(void *pmap, char *position_current, char buff_clean[64])
{
  while (1)
  {
    void *pv = position_current;
    struct iso_dir *rep = pv;
    if (rep->dir_size == 0)
      break;
    char buff_idf[rep->idf_len + 1];
    my_idf(rep,buff_idf);
    if (my_strcmp(buff_clean, buff_idf) == 0)
    {
      if (2 & rep->type)
      {
        fprintf(stderr, "entry '%s' is a directory\n", buff_clean);  
        return;
      }
      position_current = my_current_dir(pmap, position_current);
      int fd = open(buff_clean, O_CREAT | O_RDWR);
      write(fd, position_current, rep->file_size.le);
      return;
    }
    position_current += rep->dir_size; 
  }
    fprintf(stderr, "unable to find '%s' entry\n", buff_clean);
}



void isnotatty(char buff_line[64], int fd, char *position_root, struct iso_prim_voldesc *iso, void *pmap)
{
  char buff[2][64] = {{0}, {0}};
  char *position_current = position_root;
  int j =0;
  char buff_temp[64];
  clean_buffer(buff_temp);
  while(1)
  {
    j = 0;
    do 
    {
      if (read(fd, buff_temp, 1) == 0)
        return;
      if (buff_temp[0] == '\n')
        buff_temp[0] = '\0';
      buff_line[j++] = buff_temp[0];
    } while (buff_temp[0] != '\0');
    parse(buff_line, buff);
    if (exec_cmd(buff, buff_line, position_root, &position_current, iso, pmap) == 1)
      break;
    clean_buffer(buff_line);
  }
}
