#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>

void *offset_ptr(void* ptr, int64_t offset);
void parse(char buf_trash[64], char buff[][64]);
void clean_buffer(char *buff);
char *clean_buf_trash(char *buffer);
void my_getLine(char buff[][64], int fd);

#endif //! UTILS_H
