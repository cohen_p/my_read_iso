#include "iso9660.h"

#ifndef COMMAND_H
 #define COMMAND_H

 void info(struct iso_prim_voldesc *iso);
 void ls(char *repertory);
 char *cd(void *pmap, char *position_root, char *position_current,
          char buff_clean[64]);
 void cat(void *pmap, char *position_current, char buff_clean[64]);
 void get(void *pmap, char *position_current, char buff_clean[64]);
 void isnotatty(char buff_line[64], int fd, char *position_root, struct iso_prim_voldesc *iso, void *pmap);

#endif
