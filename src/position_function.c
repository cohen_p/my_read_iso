#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include "iso9660.h"
#include "string_function.h"
#include "position_function.h"
#include "utils.h"
#include "command.h"

static void help()
{
  printf("help\t: display help\n");
  printf("info\t: display volume info\n");
  printf("ls  \t: display directory content\n");
  printf("cd  \t: change current directory\n");
  printf("get \t: copy file to local directory\n");
  printf("cat \t: display file content\n");
  printf("quit\t: program exit\n");
}

void *position_mmap(int fd, char *arg1, char *arg2)
{
    struct stat my_stat;
    void *pmap = NULL;
    fstat(fd, &my_stat);
    if(fstat(fd, &my_stat) < 0)
        fprintf(stderr,"fstat error");
    if (my_stat.st_size <= ISO_BLOCK_SIZE * ISO_PRIM_VOLDESC_BLOCK)
    {
        fprintf(stderr, "%s: %s: invalid ISO9660 file\n", arg1, arg2);
        return pmap;
    }
    pmap = mmap(NULL, my_stat.st_size, PROT_READ,MAP_PRIVATE,fd,0);
    if(pmap == MAP_FAILED)
        fprintf(stderr, "mmap error");
    return pmap;
}
void my_idf(struct iso_dir *dir, char buff_idf[64])
{
  void *adr_root = dir;
  char *tmp = adr_root;
  tmp += sizeof (struct iso_dir);
  int i;
  for(i = 0; i < dir->idf_len; i++)
    buff_idf[i] = tmp[i];
  if (!(2 & dir->type))
    buff_idf[dir->idf_len - 2] = '\0';
  buff_idf[dir->idf_len] = '\0';
}
void shell(char buf_trash[64])
{
 //   if (!isatty(1))
  //  {
        printf("> ");
        fflush(0);
 //   }
    read(0,buf_trash,64);
     int i;
     for(i = 0; i < 64; i++)
        if (buf_trash[i] == '\n')
            buf_trash[i] = '\0';
}
char *my_current_dir(void *pmap, char *directory)
{
    void *adr_dir = directory;
    struct iso_dir *current_dir = adr_dir;
    char *pc = pmap;
    pc += current_dir->data_blk.le * ISO_BLOCK_SIZE;
    return pc;
}
int exec_cmd(char buff[][64], char buff_trash[64], char *position_root, char **position_current, struct iso_prim_voldesc *iso, void *pmap)
{
    if(my_strcmp(buff[0],"quit") == 0 && my_strcmp(buff[1],"") == 0)
        return 1;
    else if (my_strcmp(buff[0],"quit") == 0 && my_strcmp(buff[1],"") == 1)
      fprintf(stderr, "%s: %s: does not take an argument\n", bin_name, buff[0]);
    else if (my_strcmp(buff[0],"help") == 0 && my_strcmp(buff[1],"") == 0)
      help();
    else if (my_strcmp(buff[0],"help") == 0 && my_strcmp(buff[1],"") == 1)
      fprintf(stderr, "%s: %s: does not take an argument\n", bin_name, buff[0]);
    else if (my_strcmp(buff[0],"info") == 0 && my_strcmp(buff[1],"") == 0)
      info(iso);
    else if (my_strcmp(buff[0],"info") == 0 && my_strcmp(buff[1],"") == 1)
      fprintf(stderr, "%s: %s: does not take an argument\n", bin_name, buff[0]);
    else if (my_strcmp(buff[0],"ls") == 0 && my_strcmp(buff[1],"") == 0)
      ls(*position_current);
    else if (my_strcmp(buff[0],"ls") == 0 && my_strcmp(buff[1],"") == 1)
      fprintf(stderr, "%s: %s: does not take an argument\n", bin_name, buff[0]);
    else if (my_strcmp(buff[0],"cd") == 0) 
        *(position_current) = cd(pmap, position_root, *position_current, buff[1]);
    else if (my_strcmp(buff[0],"cat") == 0 && (my_strcmp(buff[1], "") == 1))
      cat(pmap, *position_current, buff[1]);
    else if (my_strcmp(buff[0],"cat") == 0)
      fprintf(stderr, "%s: %s: command must take an argument\n", bin_name, buff[0]);
    else if (my_strcmp(buff[0],"get") == 0 && (my_strcmp(buff[1], "") == 1))
      get(pmap, *position_current, buff[1]);
    else if (my_strcmp(buff[0],"get") == 0)
      fprintf(stderr, "%s: %s: command must take an argument\n", bin_name, buff[0]);
    else if(my_strcmp(buff[0], "") == 0)
      return 0;
    else
    {
        clean_buf_trash(buff_trash);
        fprintf(stderr, "%s: %s: unknown command\n", bin_name, buff_trash);
    }
    return 0;
}


